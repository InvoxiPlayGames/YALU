Important Note
=====
This application will no longer work for updates due to A9LH being dropped from support of Luma3DS in favor for Boot9Strap (which everyone should be running). I have no plans at this moment of updating the application to work, as the Luma3DS Updater market is oversaturated. Please for the love of all things holy use [the latest LumaUpdater fork](https://github.com/KunoichiZ/lumaupdate/releases/tag/v2.0) if you need an on-3DS CFW updater.

# Y.A.L.U.
## Yet Another Luma3DS Updater
**Y.A.L.U.** is another Luma3DS updater created in **lpp-3ds**.
It is only available for 3DS Custom Firmwares (as a .cia format) and is only used with arm9loaderhax (`arm9loaderhax.bin` is the file that gets updated)
The application downloads Luma3DS builds from [astronautlevel2's Luma3DS Build Page](https://astronautlevel2.github.io/Luma3DS/) and is lightly based from astronautlevel2's [StarUpdater](https://www.github.com/astronautlevel2/StarUpdater).
The Luma3DS Version Finding from arm9loaderhax.bin is used from StarUpdater.


### Building
To build Y.A.L.U., you need to download:
* The LPP-3DS.elf from Rinnegatamante
* makerom added to your PATH

If you have all that, you can just run `make` in the YALU folder. Alternatively, you can download [the release CIAs](https://www.github.com/InvoxiPlayGames/YALU/releases).
