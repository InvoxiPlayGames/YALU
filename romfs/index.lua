local latesturl = "http://astronautlevel2.github.io/Luma3DS/latest.zip"
local releaseurl = "http://astronautlevel2.github.io/Luma3DS/release.zip"
local updateurl = "http://invoxiplaygames.github.io/3ds/YALU/latest.cia"
local colorwhite = Color.new(255,255,255)
local colorblue = Color.new(0,128,255)
local colorred = Color.new(255,0,0)
local colorgrey1 = Color.new(175,175,175)
local colorgrey2 = Color.new(100,100,100)
local selecteditem = 1
local payloadpath = "/arm9loaderhax.bin"
local remoteversion = "undefined"

function sleep(n)
	local timer = Timer.new()
	local t0 = Timer.getTime(timer)
	while Timer.getTime(timer) - t0 <= n do end
end
function latestYALU(request)
	if Network.isWifiEnabled() then
		if request == 1 then
			return Network.requestString("http://invoxiplaygames.github.io/3ds/YALU/latestver")
		elseif request == 2 then
			return Network.requestString("http://invoxiplaygames.github.io/3ds/YALU/betaver")
		else
			return Network.requestString("http://invoxiplaygames.github.io/3ds/YALU/latestver")
		end
	else
		return "No Connection"
	end
end
function getversion(request)
	if (request ~= 1) then
		local searchString = "Luma3DS "
        local verString = ""
        local isDone = false
        if (System.doesFileExist(payloadpath) == true) then
            local file = io.open(payloadpath, FREAD)
            local fileData = io.read(file, 0, io.size(file))
            io.close(file)
            local offset = string.find(fileData, searchString)
            if (offset ~= nil) then
                offset = offset + string.len(searchString)
                while(isDone == false)
                do
                    bitRead = fileData:sub(offset,offset)
                    if bitRead == " " then
                        isDone = true
                    else
                        verString = verString..bitRead
                    end
                    offset = offset + 1
                end
                return verString
            else
                return "Error Grabbing Version"
            end
        else
            return "Luma3DS not found"
        end
    else
        if Network.isWifiEnabled() then
            return Network.requestString("http://astronautlevel2.github.io/Luma3DS/lastVer").."-"..Network.requestString("http://astronautlevel2.github.io/Luma3DS/lastCommit")
        else
            return "No Connection"
        end
	end
end

function main()
    remoteversion = getversion(1)
	Screen.waitVblankStart()
    if (Network.isWifiEnabled) then
		menu()
	else
		Screen.refresh()
        Screen.flip()
        Screen.debugPrint(5,5, "Error! Wireless disabled!", colorblue, TOP_SCREEN)
        Screen.debugPrint(5,20, "Exiting...", colorwhite, TOP_SCREEN)
        sleep(200)
        System.exit()
	end
end

function menu()
	Screen.waitVblankStart()
	Screen.refresh()
	Screen.flip()
	Screen.clear(TOP_SCREEN)
	Screen.clear(BOTTOM_SCREEN)
	Screen.debugPrint(5,5, "Yet Another Luma Updater - v1.0", colorblue, TOP_SCREEN)
	if selecteditem == 1 then
		Screen.debugPrint(5,35, "Update Luma3DS", colorred, TOP_SCREEN)
    Screen.debugPrint(5,50, "Update Luma3DS (Unstable)", colorwhite, TOP_SCREEN)
		Screen.debugPrint(5,65, "Restore Backup", colorwhite, TOP_SCREEN)
		Screen.debugPrint(5,80, "Advanced Menu...", colorwhite, TOP_SCREEN)
	elseif selecteditem == 2 then
        Screen.debugPrint(5,35, "Update Luma3DS", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,50, "Update Luma3DS (Unstable)", colorred, TOP_SCREEN)
        Screen.debugPrint(5,65, "Restore Backup", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,80, "Advanced Menu...", colorwhite, TOP_SCREEN)
	elseif selecteditem == 3 then
        Screen.debugPrint(5,35, "Update Luma3DS", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,50, "Update Luma3DS (Unstable)", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,65, "Restore Backup", colorred, TOP_SCREEN)
        Screen.debugPrint(5,80, "Advanced Menu...", colorwhite, TOP_SCREEN)
	elseif selecteditem == 4 then
        Screen.debugPrint(5,35, "Update Luma3DS", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,50, "Update Luma3DS (Unstable)", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,65, "Restore Backup", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,80, "Advanced Menu...", colorred, TOP_SCREEN)
    else
        selecteditem = 1
        Screen.debugPrint(5,35, "Update Luma3DS", colorred, TOP_SCREEN)
        Screen.debugPrint(5,50, "Update Luma3DS (Unstable)", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,65, "Restore Backup", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,80, "Advanced Menu...", colorwhite, TOP_SCREEN)
	end
	Screen.debugPrint(5,100, "Press A to select", colorwhite, TOP_SCREEN)
	Screen.debugPrint(5,115, "Press START to exit", colorwhite, TOP_SCREEN)
    Screen.debugPrint(5,145, "Installed Luma3DS Version: "..getversion(0), colorwhite, TOP_SCREEN)
    Screen.debugPrint(5,160, "Latest Luma3DS Version: "..remoteversion, colorwhite, TOP_SCREEN)
	while true do
        pad = Controls.read()
        if pad ~= oldPad then
            oldPad = pad
            if Controls.check(pad,KEY_DDOWN) then
                selecteditem = selecteditem + 1
                menu()
                return
            elseif Controls.check(pad,KEY_DUP) then
                if selecteditem == 1 then
                	selecteditem = 4
                else
                	selecteditem = selecteditem - 1
                end
                menu()
                return
            elseif Controls.check(pad,KEY_A) then
                runitem()
                return
            elseif Controls.check(pad,KEY_START) then
            	System.exit()
            end
        end
    end
end

function runitem()
	if selecteditem == 1 then
		Screen.clear(TOP_SCREEN)
		Screen.clear(BOTTOM_SCREEN)
		Screen.debugPrint(5,5, "YALU - In process...", colorblue, TOP_SCREEN)
		Screen.debugPrint(5,20, "Downloading Luma3DS Update...", colorblue, TOP_SCREEN)
		Network.downloadFile(releaseurl, "/update.zip")
        Screen.debugPrint(5,35, "Backing up payloads...", colorwhite, TOP_SCREEN)
        if System.doesFileExist(payloadpath) then
        	System.renameFile(payloadpath, payloadpath..".bak")
        end
        Screen.debugPrint(5,50, "Installing update...", colorwhite, TOP_SCREEN)
		System.extractFromZIP("/update.zip", "out/arm9loaderhax.bin", payloadpath)
        Screen.debugPrint(5,65, "Removing leftover files...", colorwhite, TOP_SCREEN)
        System.deleteFile("/update.zip")
        Screen.debugPrint(5,80, "Success! Rebooting in 5 seconds...", Color.new(0,255,0), TOP_SCREEN)
        sleep(5000)
        System.reboot()
	elseif selecteditem == 2 then
        Screen.clear(TOP_SCREEN)
        Screen.clear(BOTTOM_SCREEN)
        Screen.debugPrint(5,5, "YALU - In process...", colorblue, TOP_SCREEN)
        Screen.debugPrint(5,20, "Downloading Luma3DS Unstable Update...", colorblue, TOP_SCREEN)
        Network.downloadFile(latesturl, "/update.zip")
        Screen.debugPrint(5,35, "Backing up payloads...", colorwhite, TOP_SCREEN)
        if System.doesFileExist(payloadpath) then
            System.renameFile(payloadpath, payloadpath..".bak")
        end
        Screen.debugPrint(5,50, "Installing update...", colorwhite, TOP_SCREEN)
        System.extractFromZIP("/update.zip", "out/arm9loaderhax.bin", payloadpath)
        Screen.debugPrint(5,65, "Removing leftover files...", colorwhite, TOP_SCREEN)
        System.deleteFile("/update.zip")
        Screen.debugPrint(5,80, "Success! Rebooting in 5 seconds...", Color.new(0,255,0), TOP_SCREEN)
        sleep(5000)
        System.reboot()
	elseif selecteditem == 3 then
		Screen.clear(TOP_SCREEN)
        Screen.clear(BOTTOM_SCREEN)
        Screen.debugPrint(5,5, "YALU - In process...", colorblue, TOP_SCREEN)
        Screen.debugPrint(5,20, "Checking for backup...", colorwhite, TOP_SCREEN)
        if System.doesFileExist(payloadpath..".bak") then
            Screen.debugPrint(5,35, "Restoring backup..", colorwhite, TOP_SCREEN)
            System.renameFile(payloadpath..".bak", payloadpath)
            Screen.debugPrint(5,50, "Backup restored! Exiting...", colorwhite, TOP_SCREEN)
            sleep(1000)
            System.exit()
        else
            Screen.debugPrint(5,35, "Backup does not exist! Returning...", colorwhite, TOP_SCREEN)
            sleep(1000)
            menu()
        end
    elseif slecteditem == 4 then
        advancedmenu()
    else
        menu()
	end
end
function advancedmenu()
  selecteditem = 1
	Screen.waitVblankStart()
	Screen.refresh()
	Screen.flip()
	Screen.clear(TOP_SCREEN)
	Screen.clear(BOTTOM_SCREEN)
	Screen.debugPrint(5,5, "YALU - Advanced Menu", colorblue, TOP_SCREEN)
	if selecteditem == 1 then
		Screen.debugPrint(5,35, "Update YALU", colorred, TOP_SCREEN)
    Screen.debugPrint(5,50, "Update YALUbeta", colorwhite, TOP_SCREEN)
		Screen.debugPrint(5,65, "Uninstall YALU", colorwhite, TOP_SCREEN)
		Screen.debugPrint(5,80, "Uninstall Luma3DS", colorwhite, TOP_SCREEN)
	elseif selecteditem == 2 then
        Screen.debugPrint(5,35, "Update YALU", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,50, "Update YALUbeta", colorred, TOP_SCREEN)
        Screen.debugPrint(5,65, "Uninstall YALU", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,80, "Uninstall Luma3DS", colorwhite, TOP_SCREEN)
	elseif selecteditem == 3 then
        Screen.debugPrint(5,35, "Update YALU", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,50, "Update YALUbeta", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,65, "Uninstall YALU", colorred, TOP_SCREEN)
        Screen.debugPrint(5,80, "Uninstall Luma3DS", colorwhite, TOP_SCREEN)
	elseif selecteditem == 4 then
        Screen.debugPrint(5,35, "Update YALU", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,50, "Update YALUbeta", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,65, "Uninstall YALU", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,80, "Uninstall Luma3DS", colorred, TOP_SCREEN)
    else
        selecteditem = 1
        Screen.debugPrint(5,35, "Update YALU", colorred, TOP_SCREEN)
        Screen.debugPrint(5,50, "Update YALUdev", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,65, "Uninstall YALU", colorwhite, TOP_SCREEN)
        Screen.debugPrint(5,80, "Uninstall Luma3DS", colorwhite, TOP_SCREEN)
	end
	Screen.debugPrint(5,100, "Press A to select", colorwhite, TOP_SCREEN)
	Screen.debugPrint(5,115, "Press START to return", colorwhite, TOP_SCREEN)
  Screen.debugPrint(5,145, "Current YALU Version: 1.0 beta-07", colorwhite, TOP_SCREEN)
  Screen.debugPrint(5,160, "Latest YALU Version: "..latestYALU(1), colorwhite, TOP_SCREEN)
  Screen.debugPrint(5,160, "Latest YALUbeta Version: "..latestYALU(2), colorwhite, TOP_SCREEN)
	while true do
        pad = Controls.read()
        if pad ~= oldPad then
            oldPad = pad
            if Controls.check(pad,KEY_DDOWN) then
                selecteditem = selecteditem + 1
                advancedmenu()
                return
            elseif Controls.check(pad,KEY_DUP) then
                if selecteditem == 1 then
                	selecteditem = 4
                else
                	selecteditem = selecteditem - 1
                end
                advancedmenu()
                return
            elseif Controls.check(pad,KEY_A) then
                runadvanceditem()
                return
            elseif Controls.check(pad,KEY_START) then
            	System.exit()
            end
        end
    end
end
function runadvanceditem()
	if selecteditem == 1 then
		Screen.clear(TOP_SCREEN)
		Screen.clear(BOTTOM_SCREEN)
		Screen.debugPrint(5,5, "YALU - In process...", colorblue, TOP_SCREEN)
		Screen.debugPrint(5,20, "Downloading YALU Update...", colorblue, TOP_SCREEN)
		Network.downloadFile(updateurl, "/YALU.cia")
		Screen.debugPrint(5,35, "Installing update...", colorwhite, TOP_SCREEN)
		System.installCIA("/YALU.cia", SDMC)
		Screen.debugPrint(5,50, "Removing leftover files...", colorwhite, TOP_SCREEN)
		System.deleteFile("/YALU.cia")
		Screen.debugPrint(5,65, "Success! Exiting in 5 seconds...", Color.new(0,255,0), TOP_SCREEN)
		sleep(5000)
		System.exit()
	elseif selecteditem == 2 then
		Screen.clear(TOP_SCREEN)
		Screen.clear(BOTTOM_SCREEN)
		Screen.debugPrint(5,5, "YALU - In process...", colorblue, TOP_SCREEN)
		Screen.debugPrint(5,20, "Downloading YALUbeta Update...", colorblue, TOP_SCREEN)
		Network.downloadFile("http://invoxiplaygames.github.io/3ds/YALU/beta.cia", "/YALU.cia")
		Screen.debugPrint(5,35, "Installing update...", colorwhite, TOP_SCREEN)
		System.installCIA("/YALU.cia", SDMC)
		Screen.debugPrint(5,50, "Removing leftover files...", colorwhite, TOP_SCREEN)
		System.deleteFile("/YALU.cia")
		Screen.debugPrint(5,65, "Success! Exiting in 5 seconds...", Color.new(0,255,0), TOP_SCREEN)
		sleep(5000)
		System.exit()
	elseif selecteditem == 3 then
		Screen.clear(TOP_SCREEN)
		Screen.clear(BOTTOM_SCREEN)
		Screen.debugPrint(5,5, "YALU - In process...", colorblue, TOP_SCREEN)
		Screen.debugPrint(5,20, "Uninstalling YALU...", colorblue, TOP_SCREEN)
		Screen.debugPrint(5,50, "Due to technical limitations, you cannot", colorwhite, TOP_SCREEN)
		Screen.debugPrint(5,65, "uninstall YALU directly. Use System Settings", colorwhite, TOP_SCREEN)
		Screen.debugPrint(5,80, "Data Management to uninstall YALU.", colorwhite, TOP_SCREEN)
		Screen.debugPrint(5,95, "Returning to menu...", colorwhite, TOP_SCREEN)
		sleep(5000)
		advancedmenu()
	elseif selecteditem == 4 then
		Screen.clear(TOP_SCREEN)
		Screen.clear(BOTTOM_SCREEN)
		Screen.debugPrint(5,5, "YALU - In process...", colorblue, TOP_SCREEN)
		Screen.debugPrint(5,20, "Uninstalling Luma3DS...", colorblue, TOP_SCREEN)
		Screen.debugPrint(5,35, "Backing up payloads...", colorwhite, TOP_SCREEN)
		if System.doesFileExist(payloadpath) then
			System.renameFile(payloadpath, payloadpath..".bak")
		end
		Screen.debugPrint(5,50, "Deleting payload...", colorwhite, TOP_SCREEN)
		System.deleteFile("/arm9loaderhax.bin")
		Screen.debugPrint(5,65, "Success! Exiting in 5 seconds...", Color.new(0,255,0), TOP_SCREEN)
		Screen.debugPrint(5,80, "Your 3DS will not boot if there is no alternate!", Color.new(255,0,0), TOP_SCREEN)
		sleep(5000)
		System.exit()
	else
		advancedmenu()
	end
end
main()
